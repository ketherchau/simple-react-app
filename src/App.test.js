import React from 'react';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import App from './App';
import store from './redux/store';

test('App snapshot match', () => {
  const component = renderer.create(
    <Provider store={store}>
      <App />
    </Provider>,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
