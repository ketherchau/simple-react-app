
const fetchPostList = () => {
  return fetch('https://jsonplaceholder.typicode.com/posts')
    .then((response) => {
      return response.json();
    })
}

const fetchUserList = () => {
  return fetch('https://jsonplaceholder.typicode.com/users')
    .then((response) => {
      return response.json();
    })
}

const fetchCommentList = postId => {
  return fetch(`https://jsonplaceholder.typicode.com/comments?postId=${postId}`)
    .then((response) => {
      return response.json();
    })
}

export {
  fetchPostList,
  fetchUserList,
  fetchCommentList,
}
