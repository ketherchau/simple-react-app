import { POSTS_FETCH_SUCCEEDED, USERS_FETCH_SUCCEEDED, COMMENTS_FETCH_SUCCEEDED, FETCH_FAILED } from "../actionTypes";

const initialState = {
  postList: null,
  errorMessage: null,
};

const posts = (state = initialState, action) => {
  switch (action.type) {
    case POSTS_FETCH_SUCCEEDED: {
      const { postList } = action;
      return {
        ...state,
        postList,
      };
    }
    case USERS_FETCH_SUCCEEDED: {
      const { userList } = action;

      const postList = state.postList;

      postList?.forEach(post => {
        userList?.forEach(user => {
          if (user.id === post.userId) {
            return post.email = user.email
          }
        })
      })

      return {
        ...state,
        postList,
      };
    }
    case COMMENTS_FETCH_SUCCEEDED: {
      const { commentList, postId } = action;

      const postList = state.postList;

      postList?.forEach(post => {
        if (post.id === postId) {
          return post.comments = commentList
        }
      })

      return {
        ...state,
        postList,
      };
    }
    case FETCH_FAILED: {
      const { errorMessage } = action;
      return {
        ...state,
        errorMessage,
      };
    }
    default:
      return state;
  }
}

export default posts;
