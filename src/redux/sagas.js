import { call, put, takeEvery, takeLatest, all } from 'redux-saga/effects'
import * as Api from '../services/api'
import { POSTS_FETCH_SUCCEEDED, USERS_FETCH_SUCCEEDED, COMMENTS_FETCH_SUCCEEDED, FETCH_FAILED, POST_FETCH_REQUESTED, USER_FETCH_REQUESTED, COMMENT_FETCH_REQUESTED } from "./actionTypes";

// worker Saga: will be fired on POST_FETCH_REQUESTED actions
function* fetchPostList(action) {
   try {
      const postList = yield call(Api.fetchPostList);
      yield put({type: POSTS_FETCH_SUCCEEDED, postList });
   } catch (e) {
      yield put({type: FETCH_FAILED, errorMessage: e.message});
   }
}

function* fetchUserList(action) {
   try {
      const userList = yield call(Api.fetchUserList);
      yield put({type: USERS_FETCH_SUCCEEDED, userList });
   } catch (e) {
      yield put({type: FETCH_FAILED, errorMessage: e.message});
   }
}

function* fetchCommentList(action) {
   try {
      const commentList = yield call(Api.fetchCommentList, action.id);
      yield put({type: COMMENTS_FETCH_SUCCEEDED, commentList, postId: action.id });
   } catch (e) {
      yield put({type: FETCH_FAILED, errorMessage: e.message});
   }
}

/*
  Starts fetchPosts on each dispatched `POST_FETCH_REQUESTED` action.
  Allows concurrent fetches of posts.
*/
export function* postFetchRequestAsync() {
  yield takeEvery(POST_FETCH_REQUESTED, fetchPostList);
}

function* userFetchRequestAsync() {
  yield takeEvery(USER_FETCH_REQUESTED, fetchUserList);
}

function* commentFetchRequestAsync() {
  yield takeLatest(COMMENT_FETCH_REQUESTED, fetchCommentList);
}


export default function* rootSaga() {
  yield all([
    postFetchRequestAsync(),
    userFetchRequestAsync(),
    commentFetchRequestAsync(),
  ])
}
