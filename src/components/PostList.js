import React from 'react';
import { Row, Col, Avatar, Button, Card, Empty, Skeleton } from 'antd';
import { useDispatch, useSelector } from "react-redux";
// import { selectPostList } from "../redux/selectors";
import { COMMENT_FETCH_REQUESTED } from "../redux/actionTypes";

const PostList = () => {
  const [data, setData] = React.useState([]);
  const [toggleComment, setToggleComment] = React.useState([]);
  const dispatch = useDispatch()
  const posts = useSelector(state => state.posts)

  React.useEffect(() => {
    if (posts?.postList) {
      const data = posts.postList.filter((p, i) => i < 10)
      setData(data)
    }
  }, [posts])

  const onClickToggleComment = id => {
    dispatch({ type: COMMENT_FETCH_REQUESTED, id })
    if (toggleComment.includes(id)) {
      const index = toggleComment.indexOf(id)
      toggleComment.splice(index, 1)
    } else {
      toggleComment.push(id)
    }
    setToggleComment(toggleComment)
  }

  if (!data.length) {
    return <Empty />
  }

  return (
    <Row gutter={24}>
    {
      data.map((d, i) => (
        <Col span={8} key={i}>
          <Card
            className="App-card"
            title={d.title}
            >
              <Row gutter={24}>
                <p>Body: {d.body}</p>
                <p>Email: {d.email}</p>
                <Col span={24}>
                  <Button type="primary" className="App-card-button" onClick={() => onClickToggleComment(d.id)}>
                    {
                      toggleComment.includes(d.id)
                        ? 'Hide Comment'
                        : 'Show Comment'
                    }
                  </Button>
                </Col>
              </Row>

              {
                toggleComment.includes(d.id) && (
                  <Skeleton loading={!toggleComment.includes(d.id)} avatar active>
                    Comments:
                    {
                      d.comments?.map((comment, index) => (
                        <Card.Meta
                          key={index}
                          avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                          title={`Email: ${comment?.email}`}
                          description={`Body: ${comment?.body}`}
                          className="App-card-meta"
                        />
                      ))
                    }
                  </Skeleton>
                )
              }
            </Card>
          </Col>
        ))
    }
    </Row>
  )
}

export default PostList;
