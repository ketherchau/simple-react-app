import React from 'react';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import PostList from './PostList';
import App from '../App';
import store from '../redux/store';

test('PostList snapshot match', () => {
  const component = renderer.create(
    <Provider store={store}>
      <App>
        <PostList />
      </App>
    </Provider>,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
