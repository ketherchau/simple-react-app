import React from 'react';
import { PageHeader, Descriptions, BackTop } from 'antd';
import { useDispatch } from "react-redux";
import PostList from './components/PostList';
import { POST_FETCH_REQUESTED, USER_FETCH_REQUESTED } from "./redux/actionTypes";
import 'antd/dist/antd.css';
import './App.css';

const App = () => {
  const dispatch = useDispatch()

  React.useEffect(() => {
    dispatch({ type: POST_FETCH_REQUESTED })
    dispatch({ type: USER_FETCH_REQUESTED })
  }, [dispatch])

  return (
    <div className="App">
      <PageHeader
        className="site-page-header"
        title="Simple React App"
        subTitle="Post List fetched from jsonplaceholder"
      />
      <Descriptions title="Post List" />
      <PostList />
      <BackTop />
    </div>
  );
}

export default App;
